package com.multipanelayout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tanvi.multipanelayout.R;

public class MovieDetailsFragment extends Fragment {

    final static String ARG_POSITION = "position";
    int mCurrentPosition = -1;
    TextView tv_movie_details;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
        }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.movie_details_fragment, container, false);
        tv_movie_details = (TextView) view.findViewById(R.id.tv_movie_details);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();


        Bundle args = getArguments();
        if (args != null) {
            // Set movie details based on argument passed in
            updateMovieDetailsView(args.getInt(ARG_POSITION));
        } else if (mCurrentPosition != -1) {
            // Set movie details based on saved instance state defined during onCreateView
            updateMovieDetailsView(mCurrentPosition);
        }
    }

    public void updateMovieDetailsView(int position) {
        tv_movie_details.setText(com.multipanelayout.MovieData.MovieDetails[position]);
        mCurrentPosition = position;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current movie selection in case we need to recreate the fragment
        outState.putInt(ARG_POSITION, mCurrentPosition);
    }
}
